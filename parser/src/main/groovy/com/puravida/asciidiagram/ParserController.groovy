package com.puravida.asciidiagram

import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.MediaType
import groovy.transform.CompileStatic

import javax.inject.Inject

@CompileStatic
@Controller("/")
class ParserController {

    @Inject
    AdocEngine adocEngine

    @Get(value = "/{render}/{format}/{adoc}")
    HttpResponse<?> render(String render, String format, String adoc){
        byte[] result = null;

        format = "png";

        String decoded = URLDecoder.decode( new String(adoc.decodeBase64()),"UTF-8")

        String id = decoded.md5()

        result = adocEngine.buildImage(render, id, format, decoded)

        HttpResponse.ok(result)
                .contentType(contentType(format))
    }

    String contentType(String format){
        switch (format){
            case "png":
                return "image/png"
            case "svg":
                return "image/svg"
            default:
                return "text/html"
        }
    }

}
