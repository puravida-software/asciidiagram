package com.puravida.asciidiagram

import groovy.transform.CompileStatic

import javax.annotation.PostConstruct;

import org.asciidoctor.Asciidoctor

import javax.inject.Singleton;

import static org.asciidoctor.Asciidoctor.Factory.create;
import static org.asciidoctor.AttributesBuilder.attributes;
import static org.asciidoctor.OptionsBuilder.options;

@CompileStatic
@Singleton
class AdocEngine {

    Asciidoctor asciidoctor

    @PostConstruct
    void init() {
        asciidoctor = create()
        asciidoctor.requireLibrary("asciidoctor-diagram")

    }

    String parseDocument(String document){
        Map<String,Object> attributes = [:]
        attributes.imagesdir = 'build'
        String html = asciidoctor.convert(
                document,
                options().attributes(attributes).backend("html5").get())
        html
    }

    byte[] buildImage(String render, String imageName, String format, String adoc) throws Exception {

        String full = String.join("\n",
                "[" + render + "," + imageName + "]",
                "....",
                adoc,
                "...."
        )

        System.out.println(full)

        String html = parseDocument(full)

        File f = new File("build/${imageName}.png")

        byte[] result = f.bytes

        f.delete()

        result
    }


}
