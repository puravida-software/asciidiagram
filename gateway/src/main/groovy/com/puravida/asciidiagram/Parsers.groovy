package com.puravida.asciidiagram

import groovy.transform.CompileStatic
import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.RxHttpClient

import javax.inject.Inject
import javax.inject.Singleton

@CompileStatic
@Singleton
class Parsers {

    RxHttpClient plantuml

    RxHttpClient common

    RxHttpClient node

    RxHttpClient python

    Parsers(ApplicationContext applicationContext, ParsesConfiguration parsesConfiguration){
        plantuml = applicationContext.createBean(RxHttpClient, parsesConfiguration.plantuml)
        common = applicationContext.createBean(RxHttpClient, parsesConfiguration.common)
        node = applicationContext.createBean(RxHttpClient, parsesConfiguration.node)
        python = applicationContext.createBean(RxHttpClient, parsesConfiguration.python)
    }

    RxHttpClient getParser(String render){
        switch( render ){
            case 'plantuml':
                return plantuml
            case 'mermaid':
                return node
            case 'nwdiag':
            case 'syntrax':
                return python
            default:
                return common
        }
    }
}
