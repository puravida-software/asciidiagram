package com.puravida.asciidiagram
import groovy.transform.CompileStatic
import io.micronaut.context.annotation.ConfigurationProperties
import io.micronaut.context.annotation.Requires

@CompileStatic
@ConfigurationProperties(ParsesConfiguration.PREFIX)
@Requires(property = ParsesConfiguration.PREFIX)
class ParsesConfiguration {

    public static final String PREFIX = "parsers"

    String common
    String plantuml
    String node
    String python
}
