package com.puravida.asciidiagram

import groovy.transform.CompileStatic
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.client.DefaultHttpClient
import io.micronaut.http.uri.UriTemplate
import io.reactivex.Flowable
import io.reactivex.Maybe
import org.codehaus.groovy.runtime.EncodingGroovyMethods

import javax.inject.Inject
import java.nio.ByteBuffer

@CompileStatic
@Controller("/")
class GatewayController {

    @Inject
    Parsers parsers

    @Get(value = "/{render}/{format}/{adoc}")
    HttpResponse<?> render(String render, String format, String adoc){

        String decoded = URLDecoder.decode( new String(adoc.decodeBase64()),"UTF-8")
        String encoded = URLEncoder.encode( new String(decoded.getBytes('UTF-8')), "UTF-8").bytes.encodeBase64()

        HttpRequest<ByteBuffer> req = HttpRequest.GET("/$render/$format/$encoded")
        HttpResponse<byte[]> response = parsers.getParser(render).toBlocking().exchange(req,byte[])

        HttpResponse.ok(response.body())
                .contentType(response.contentType.get())
    }


}
