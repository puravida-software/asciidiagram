= AsciiDiagram
Jorge Aguilera <jorge.aguilera@puravida-software.com>

AsciiDiagram is an HTTP server to convert a GET into a diagram.

The main idea is you perfom a GET to the endpoint `/{parser}/{format}/{text}` and the
end-point will return a response with the generated diagram

It supports different languages/parsers as:

- Graphviz
- Ditaa
- PlantUML
- Mermaid
- Syntrax
- Nwdiag

In the same way , every parsers support different formats as PNG, Text, etc

To allow huge text in the request, the `text` must be `compress`. For example you can use
this javascript:

[source,javascript]
----
var adoc = document.getElement('adoc').text;
var parser = 'plantuml';
var format = 'png';
var url = host + parser + '/' + format + '/' + encodeURIComponent(btoa(adoc));
document.getElement('img').src = url;
----

== Architecture

AsciiDiagram architecture is showing in this diagram:

[plantuml]
....

!define SPRITESURL https://raw.githubusercontent.com/rabelenda/cicon-plantuml-sprites/v1.0/sprites
!includeurl SPRITESURL/nodejs.puml
!includeurl SPRITESURL/kafka.puml
!includeurl SPRITESURL/tomcat.puml
!includeurl SPRITESURL/server.puml
!includeurl SPRITESURL/python.puml

title AsciiDiagram Architecture

skinparam monochrome true

rectangle "<$nodejs>\nwebapp" as webapp
queue "<$kafka>\nAPI" as api
file "<$server>\nStorage" as repo
rectangle "<$tomcat>\ncommon" as common
rectangle "<$tomcat>\nplantuml" as plantuml
rectangle "<$tomcat>\npython3" as python3
rectangle "<$tomcat>\nnode8" as node8

webapp --> api
api -> repo
repo -> api
api --> common
api --> plantuml
api --> python3
api --> node8

....

As you can see, we have an entry-point `API` who redirect the request to the right parser.

